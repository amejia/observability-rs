# Observability

This is a Rust library for initializing a tracing system with OpenTelemetry and Honeycomb.
The library provides functions for initializing a tracing subscriber and an OpenTelemetry tracer using a configuration file.

## Configuration

The configuration structs derive from serde, therefore can be deserialized from your preferred format.
~~~ron
# tracing_config.ron
(
    level: "DEBUG",
    opentelemetry: Some(
        (
            endpoint: "ENDPOINT",
            api_key: "API_KEY",
            dataset: "DATASET",
        )
    ),
)
~~~

## Usage

To use the library, call the init_tracing() function with the configuration struct as argument:
```rust

extern crate ron;
use observability::{ TracingConfig , OpenTelemetryConfig};

fn main() -> anyhow::Result<()> {

    let config_file = std::fs::read_to_string("./tracing_config.ron")?;
    let config: TracingConfig = ron::from_str(config_file.as_str())?;

    let init_res = init_tracing(&config);

    # Write here your code...
}

```

The `init_tracing()` function initializes the tracing system with the specified configuration.
The function sets the global tracing subscriber and propagator. If an OpenTelemetryConfig is provided installs the OpenTelemetry pipeline.
