mod error;
mod otlp;

use opentelemetry::global;
use opentelemetry::sdk::propagation::TraceContextPropagator;
use opentelemetry::sdk::trace::Tracer;

use otlp::OTLPConfig;
use tracing::{subscriber, Level};
use tracing_opentelemetry::OpenTelemetryLayer;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::registry::LookupSpan;
use tracing_subscriber::{prelude::*, EnvFilter};

use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};

use anyhow::Result;

#[serde_as]
#[derive(Debug, Deserialize)]
pub struct ConsoleConfig {
    // Serialize with Display, deserialize with FromStr
    #[serde_as(as = "DisplayFromStr")]
    #[serde(default = "ConsoleConfig::get_default_level")]
    level: Level,
    #[serde(default)]
    with_target: bool,
    #[serde(default)]
    with_thread_ids: bool,
    #[serde(default)]
    with_thread_names: bool,
}
impl ConsoleConfig {
    fn get_default_level() -> Level {
        Level::INFO
    }
}
impl Default for ConsoleConfig {
    fn default() -> Self {
        Self {
            level: ConsoleConfig::get_default_level(),
            with_target: Default::default(),
            with_thread_ids: Default::default(),
            with_thread_names: Default::default(),
        }
    }
}

#[serde_as]
#[derive(Debug, Deserialize, Default)]
pub struct TracingConfig {
    // Serialize with Display, deserialize with FromStr
    #[serde(default)]
    console: ConsoleConfig,
    #[serde(default)]
    opentelemetry: Option<OTLPConfig>,
}

pub fn init_console_subscriber<S>(
    config: &TracingConfig,
) -> Box<dyn tracing_subscriber::Layer<S> + Send + Sync + 'static>
where
    S: tracing::Subscriber,
    for<'a> S: LookupSpan<'a>,
{
    dbg!(&config.console);
    // Configure a custom event formatter
    let format = tracing_subscriber::fmt::format()
        // .with_level(false) // don't include levels in formatted output
        .with_target(config.console.with_target) // don't include targets
        .with_thread_ids(config.console.with_thread_ids) // include the thread ID of the current thread
        .with_thread_names(config.console.with_thread_names) // include the name of the current thread
        .compact(); // use the `Compact` formatting style.

    tracing_subscriber::fmt::layer()
        .event_format(format)
        .with_filter(LevelFilter::from_level(config.console.level))
        // Box the layer as a type-erased trait object, so that it can
        // be pushed to the `Vec`.
        .boxed()
}

// pub fn get_otel_layer<S>(tracer: Tracer) -> Option<OpenTelemetryLayer<S, Tracer>>
// where
//     S: tracing::Subscriber + for<'span> LookupSpan<'span>,
// {
//     // How to build the layer by chaining the _tracers vector items.
//     Some(tracing_opentelemetry::layer().with_tracer(tracer))
// }

pub fn get_otel_layers<S>(mut tracers: Vec<Tracer>) -> Option<OpenTelemetryLayer<S, Tracer>>
where
    S: tracing::Subscriber + for<'span> LookupSpan<'span>,
{
    // How to build the layer by chaining the _tracers vector items.
    tracers.pop().map(|first_tracer| {
        let first_tracer_layer = tracing_opentelemetry::layer().with_tracer(first_tracer);

        tracers.into_iter().fold(first_tracer_layer, |l, t| {
            tracing::info!("Setting up {:#?} on layer", t);
            l.with_tracer(t)
        })
    })
}

pub fn init_tracing(config: &TracingConfig) -> Result<()> {
    let fmt_layer = init_console_subscriber(config);

    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    let messages: Vec<String> = vec![];

    let otel_layer = config
        .opentelemetry
        .as_ref()
        .map(|otlp_config| match otlp::get_tracer(otlp_config) {
            Ok(tracer) => {
                tracing::info!("Opentelemetry set up!");
                Some(tracing_opentelemetry::layer().with_tracer(tracer))
            }
            Err(e) => {
                tracing::error!("OTLPError {:?}", e);
                None
            }
        })
        .unwrap_or(None);

    // use that subscriber to process traces emitted after this point
    let layered_subscriber = tracing_subscriber::registry::Registry::default()
        .with(filter_layer)
        .with(fmt_layer)
        .with(otel_layer);

    subscriber::set_global_default(layered_subscriber)?;

    // # TODO: Verificar el cambio con y sin este propagador
    opentelemetry::global::set_text_map_propagator(TraceContextPropagator::new());

    for msg in messages.iter() {
        tracing::info!("{}", msg);
    }

    tracing::info!("Tracing initialized.");

    Ok(())
}

pub async fn graceful_shutdown() {
    std::thread::sleep(std::time::Duration::from_secs(5));
    global::shutdown_tracer_provider();
}

#[cfg(test)]
mod tests {

    use crate::otlp::HoneycombExporterConfig;

    use super::*;

    #[test]
    fn test_init_tracing() {
        let config = TracingConfig {
            console: ConsoleConfig::default(),
            opentelemetry: Some(otlp::OTLPConfig::Honeycomb(HoneycombExporterConfig {
                endpoint: "ENDPOINT".into(),
                api_key: "API_KEY".into(),
                dataset: "DATASET".into(),
            })),
        };

        let init_res = init_tracing(&config);
        assert!(init_res.is_ok());
    }
}
