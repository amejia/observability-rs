use crate::error::OTLPError;
use std::collections::HashMap;

use opentelemetry::sdk::trace::{self, Sampler, Tracer};
use opentelemetry::sdk::Resource;
use opentelemetry_otlp::{self, WithExportConfig};

use serde::Deserialize;
use serde_with::serde_as;

type Endpoint = String;
type APIKey = String;
type ExtraValue = String;

#[derive(Debug, Deserialize, Clone)]
pub enum OTLPConfig {
    Honeycomb(HoneycombExporterConfig),
    Aspecto(AspectoExporterConfig),
}

impl OTLPConfig {
    fn get_endpoint(&self) -> Endpoint {
        match self {
            OTLPConfig::Honeycomb(c) => c.endpoint.clone(),
            OTLPConfig::Aspecto(c) => c.endpoint.clone(),
        }
    }
}

impl From<OTLPConfig> for opentelemetry_otlp::SpanExporterBuilder {
    fn from(config: OTLPConfig) -> Self {
        let headers = match config.clone() {
            OTLPConfig::Honeycomb(c) => HashMap::from([
                ("x-honeycomb-dataset".into(), c.dataset),
                ("x-honeycomb-team".into(), c.api_key),
            ]),
            OTLPConfig::Aspecto(c) => HashMap::from([("Authorization".into(), c.api_key)]),
        };
        tracing::info!("{:?}", &headers);

        let exporter = opentelemetry_otlp::new_exporter()
            .http()
            .with_endpoint(config.get_endpoint())
            .with_http_client(reqwest::Client::default())
            .with_headers(headers)
            .with_timeout(std::time::Duration::from_secs(30));

        opentelemetry_otlp::SpanExporterBuilder::Http(exporter)
    }
}

#[serde_as]
#[derive(Debug, Deserialize, Clone)]
pub struct HoneycombExporterConfig {
    pub endpoint: Endpoint,
    pub api_key: APIKey,
    pub dataset: ExtraValue,
}

#[serde_as]
#[derive(Debug, Deserialize, Default, Clone)]
pub struct AspectoExporterConfig {
    pub endpoint: Endpoint,
    pub api_key: APIKey,
}

pub fn get_tracer(otlp_config: &OTLPConfig) -> Result<Tracer, OTLPError> {
    // Err(OTLPError::ConfigurationError)

    let otlp_exporter: opentelemetry_otlp::SpanExporterBuilder = otlp_config.to_owned().into();
    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(otlp_exporter)
        .with_trace_config(
            trace::config()
                .with_sampler(Sampler::AlwaysOn)
                .with_resource(Resource::default()),
        )
        .install_batch(opentelemetry::runtime::Tokio)?;
    Ok(tracer)
}
