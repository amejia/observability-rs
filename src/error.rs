#![allow(dead_code)]
use opentelemetry::trace::TraceError;
use thiserror::Error;

#[derive(Error, Debug)]
#[error("OTLP Error")]
pub enum OTLPError {
    #[error("Configuration error")]
    ConfigurationError,
    #[error("Trace error: {0}")]
    TraceError(#[from] TraceError),
}
